const finiteAutomation = require('./finite-automation');

describe('Invalid Inputs', function() {
  it('should throw an error for NaN', function() {
    try {
      finiteAutomation('-1hello!');
    } catch (e) {
      expect(e.message).toMatch(/^Invalid Input/)
    }
  })

  it('should throw an error for greater than one', function() {
    try {
      finiteAutomation('222');
    } catch (e) {
      expect(e.message).toMatch(/^Invalid Input/)
    }
  })

  it('should throw an error for empty input', function() {
    try {
      finiteAutomation('   ');
    } catch (e) {
      expect(e.message).toMatch(/^Invalid Input/)
    }
  })
})

describe('Valid Inputs', function() {
  it('should return S0 and the remainder of 0', function() {
    const result = finiteAutomation('110');
    expect(result).toHaveProperty('finalState', 0);
    expect(result).toHaveProperty('remainder', 0);
  })

  it('should return S1 and the remainder of 1', function() {
    const result = finiteAutomation('1010');
    expect(result).toHaveProperty('finalState', 1);
    expect(result).toHaveProperty('remainder', 1);
  })
  
  it('should return S2 and the remainder of 2', function() {
    const result = finiteAutomation('01110');
    expect(result).toHaveProperty('finalState', 2);
    expect(result).toHaveProperty('remainder', 2);
  })
})

