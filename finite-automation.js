module.exports = function finiteAutomation(input) {
	// INITIAL STATES
	var curState = 0;
	var decimal = 0;
	var exp = input.length;
	for (var i = 0; i < input.length; ++i) {

		const num = parseInt(input[i]);
		if (isNaN(num) || num > 1) {
			throw new Error('Invalid Input: only 1s and 0s are accepted');
		}
		--exp;

		if (curState === 0 && num === 1) {
			decimal += Math.pow(2, exp);
			curState = 1;
		} else if (curState === 1) {
			if (num === 1) {
				decimal += Math.pow(2, exp);
				curState = 0;
			} else {
				curState = 2;
			}
		} else if (curState === 2) {
			if (num === 1) {
				decimal += Math.pow(2, exp);
			} else {
				curState = 1;
			}
		}
	}
	return { finalState: curState, remainder: decimal%3 };
}

