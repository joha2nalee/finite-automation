const { stdin, stdout } = process;
const finiteAutomation = require('./finite-automation');

function prompt(question) {
	return new Promise(function(resolve, reject) {
		stdin.resume();
		stdout.write(question);
		stdin.on('data', data => resolve(data.toString().trim()));
		stdin.on('error', err => reject(err));
	});
}

async function main() {
	try {
		const input = await prompt("What is your input string? ");
		const result = finiteAutomation(input);
		console.log(`S${result.finalState} = ${result.remainder}`);
	} catch (error) {
		console.error(error);
	}

	process.exit();
}

main();
