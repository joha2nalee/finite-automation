This is a program that embodies the finite state automation which provides the remainder of a division in 3.

A finite automation (FA) is a 5 tuple (Q, Σ, q0, F, δ) where

- Q is a finite set of states
- Σ is a finite input alphabet
- q0 ∈ Q is the initial state
- F ⊆ Q is the set of accepting/final states
- δ:Q×Σ→Q is the transition function

For any element q of Q and any symbol σ∈Σ, we interpret δ(q,σ) as the state to which the FA moves, if it is in state q and receives the input σ.

- Q = (S0, S1, S2)
- Σ = (0, 1)
- q0 = S0
- F = (S0, S1, S2)

FSM will have 3 states

- S0: Starting and final state (output value of 0)
- S1: Final state (output value of 1)
- S2: Final state (output value of 2)

```
// Install Dependencies
npm i

// Run the program
npm start

// Run test
npm run test
```
